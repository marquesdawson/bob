import shell from 'shelljs';
import config from '../config';

const brArg = process.argv[2] && process.argv[2].match(/(?:^br=)([\w-]+)$/i);
const branch = (brArg && brArg[1]) || config.branchToUpdate || 'dev-branch';

/**
 * @param name {String} The project name
 * @param dir {String} The project directory
 * @param shouldStash {Boolean} OPTIONAL - Whether it should stash before merge
 */

export const update = (name, dir, shouldStash = false) => {
	shell.echo(`Updating ${name}`);
	shell.cd(dir);
	shouldStash && shell.exec('git stash');
	shell.exec(`git fetch ${config.remoteName}`);
	if (shell.exec(`git checkout ${branch}`).code !== 0) shell.exit(1);
	if (shell.exec(`git merge ${config.remoteName}/${branch}`).code !== 0) shell.exit(1);
	shouldStash && shell.exec('git stash pop');
	shell.echo(' ');
	shell.echo(`Finished updating ${name}`);
};

/**
 * @param path {String} The file's full path
 * @param isWildCard {Boolean} OPTIONAL - Whether its a wildcard
 */
export const remove = (path, isWildCard = false) => {
	let test = isWildCard ? shell.ls(path).length : shell.test('-e', path);
	let filename = path.match(/[\w*-]+\.[a-z]{3}$/g);
	if (test) {
		shell.rm('-rf', path); // '-rf' makes it so this supports deleting files or directories
		shell.echo(`Removed ${filename}`);
	}
};

/**
 * @param name {String} The project name
 * @param dir {String} The project directory
 * @param commands {Array} An array of commands to execute to build
 */
export const build = (name, dir, commands) => {
	shell.echo(`*************** Building ${name} ***************`);
	shell.cd(dir);

	commands.forEach(command => {
		if (shell.exec(command).code !== 0) {
			shell.echo(`*************** ${name} build failed ***************`);
			shell.exit(1);
		}
	});

	shell.echo(' ');
};

/**
 * @param filePath {String} The file path for the file you want copied
 * @param targetPath {String} The target path for the file ypu want copied over
 * @param isWildCard {Boolean} OPTIONAL - Whether its a wildcard
 */
export const copy = (filePath, targetPath, isWildCard = false) => {
	let test = isWildCard ? shell.ls(filePath).length : shell.test('-e', filePath);
	if (test) {
		if (shell.cp(filePath, targetPath).code !== 0) {
			shell.echo(`Error copying - ${filePath}`);
			shell.exit(1);
		}
	} else {
		shell.echo(`Error cp - ${filePath} does not exist`);
		shell.exit(1);
	}
};
