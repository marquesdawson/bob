import shell from 'shelljs';
import config from '../config';
import { build, copy } from '../helpers/helpers';

// Build mts-sage, then copy exploded war to mts-sage-jboss deploy directory -- antrun:run does the initial copy, so mts-sage doesn't have a 'copy process' further below
build('mts-sage', config.mtsSageDir, ['mvn clean install package -DskipTests=true', 'mvn antrun:run']);

shell.echo('*************** exploded war copied ***************');
