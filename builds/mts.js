// Dependencies
import shell from 'shelljs';
import config from '../config';
import { update, remove, build, copy } from '../helpers/helpers';

// Directories
const jbossDeployDir = `${config.mtsJbossDir}/server/local/deploy`;
const jbossDeployLastDir = `${jbossDeployDir}/deploy.last/`;
const tribeTargetDir = `${config.mtsTribeDir}/tribe-handyman/target`;
const dataServiceTargetDir = `${config.mtsCmsDataServiceDir}/mts-cms-data-service-ear/target`;

if (process.env.UPDATE === 'true') {
	shell.echo('*************** Updating Projects ***************');

	update('Catman', config.mtsCatmanDir);
	update('Tribe', config.mtsTribeDir);
	update('MTS Data Service', config.mtsCmsDataServiceDir);
}

shell.echo('*************** Removing old .wars / .ears ***************');

remove(`${config.mtsSageDir}/sage-war/target/sage.war`);
remove(`${dataServiceTargetDir}/mts-cms-data-service.ear`);
remove(`${tribeTargetDir}/tribe-handyman-*.war`, true);
remove(`${config.mtsCatmanDir}/dist/cegCategories.war`);
remove(`${jbossDeployLastDir}/sage.war`);
remove(`${jbossDeployDir}/mts-cms-data-service.ear`);
remove(`${jbossDeployLastDir}tribe-handyman-*.war`, true);
remove(`${jbossDeployLastDir}cegCategories.war`);

shell.echo('Finished : Removing old .wars / .ears');

shell.echo('*********************************************');
shell.echo('*********************************************');
shell.echo('*********************************************');
shell.echo('*********************************************');
shell.echo(' ');
shell.echo('Hopefully you manually updated the mts-sage repo');
shell.echo(' ');
shell.echo('*********************************************');
shell.echo('*********************************************');
shell.echo('*********************************************');
shell.echo('*********************************************');
shell.echo(' ');

// Begin Building
build('mts-cms-data-service', config.mtsCmsDataServiceDir, ['mvn clean package -Dmaven.test.skip=true']);

build('mts-catman', config.mtsCatmanDir, [
	'ant -buildfile build.xml clean',
	'ant -buildfile build.xml compile',
	'ant -buildfile build.xml war'
]);

build('mts-tribe', config.mtsTribeDir, ['mvn clean install package -DskipTests=true']);

// Build mts-sage, then copy exploded war to mts-sage-jboss deploy directory -- antrun:run does the initial copy, so mts-sage doesn't have a 'copy process' further below
build('mts-sage', config.mtsSageDir, ['mvn clean install package -DskipTests=true -Dwebpack.env=build:dev', 'mvn antrun:run']);

shell.echo('*************** Copying Files ***************');

// Copy process
copy(`${dataServiceTargetDir}/mts-cms-data-service.ear`, jbossDeployDir);
copy(`${tribeTargetDir}/[tribe-handyman-]\*.war`, jbossDeployLastDir, true);
copy(`${config.mtsCatmanDir}/dist/cegCategories.war`, jbossDeployLastDir);
shell.echo('Finished : Copying Files');
