import shell from 'shelljs';
import config from '../config';
import { update, build } from '../helpers/helpers';

if (process.env.UPDATE === 'true') {
	update('eonline-service', config.serviceDir);
	update('eonline-commons-client', config.cciDir, true);
}

build('eonline-commons-client', config.cciDir, [
	'mvn -DskipTests clean remote-resources:bundle install'
]);
build('eonline-service', config.serviceDir, [
	'mvn -DskipTests clean remote-resources:bundle install'
]);
