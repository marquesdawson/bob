![alt text](/images/bob_03.png)

# Bob - The Builder Tool

## Prerequisites for both EOL & MTS
1. NODE v6+ 
2. You have maven installed & `mvn` is accessible via the command line

## Prerequisites for MTS
1. You have ant installed & `ant` is accessible via the command line
2. You're on Java 1.7 & Maven 3.2.X 

## Installation
Once cloned or forked : 
```bash
yarn install 
```
Then open `config.js` and update to point to YOUR directories
```js
// config.js
export default {
	// To build Sage 
	mtsSageDir : '~/Documents/eol/workspace/mts-sage',
	mtsCatmanDir : '~/Documents/eol/workspace/mts-catman',
	mtsTribeDir : '~/Documents/eol/workspace/mts-tribe', 
	mtsCmsDataServiceDir : '~/Documents/eol/workspace/mts-cms-data-service',
	mtsJbossDir : '~/Documents/eol/eol-servers/mts-sage-jboss',
	// To build CCI & Service for EOL Projects (NOT NEEDED TO BUILD SAGE)
	serviceDir : '~/Documents/eol/workspace/eonline-service',
	cciDir : '~/Documents/eol/workspace/eonline-commons-client',
	// Misc Options
	branchToUpdate : 'dev-branch',
	remoteName : 'upstream'
};
```
## To Build MTS:

#### Required Configuration
For Sage hot deployments to work you **must** update 2 files:
1. `mts-sage/pom.xml`: Update the value of `jboss.serverDir` to the absolute path of the local server directory in the `mts-sage-jboss` file structure. Example: `<jboss.serverDir>/Users/matt/git/mts-sage-jboss/server/local</jboss.serverDir>`
2. `mts-sage/sage-war/pom.xml`: Uncomment the `webappDirectory` node in this config file. Example: `<webappDirectory>${jboss.deploylastDir}/sage.war</webappDirectory>`

#### Option 1
Builds & copies over exploded .war for the mts-sage/sage-war front end and bundled .war / .ear for other modules / dependencies :
```js
yarn mts:build
```
#### Option 2
 Updates repos from (config.remoteName) & (config.branchToUpdate), then builds & copies over exploded mts-sage/sage-war front end .war and bundled .war / .ear for other modules and dependencies
```js
yarn mts:build:u
```
`WARNING` : In order to avoid possible merge conflicts, this does NOT update `mts-sage` for you. You need to manually update.

#### Option 3
Builds `mts-sage` & copies over just the exploded .war for mts-sage/sage-war and bundled .war / .ear for other modules in the mts-sage repository :
```js
yarn mts:rebuild
```
## To Build for EOL (CCI & Service):
#### Option 1
Builds CCI and Service
```js
yarn eol:build
```
#### Option 2
 Updates CCI & Service from upstream (config.branchToUpdate) and then builds both
```js
yarn eol:build:u
```

## CLI Options
Use br=branchToUpdate to switch between branches when you want to update and build
```js
// ex. 
yarn eol:build:u br=RC-20171115
```
